<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Exprience::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'author_id' => 1,
        'description' => $faker->text,
        'title' => $title,
        'body' => $faker->text,
        'image_preview' => 'image.png',
        'slug' => mb_substr(\Illuminate\Support\Str::slug($title), 0 , 8)
    ];
});
