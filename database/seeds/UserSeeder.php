<?php

use App\Article;
use App\Exprience;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Exprience::truncate();
        User::truncate();
        Article::truncate();
        factory(User::class , 5)->create()->each(function(User $user){
            $user->experiences()->save(factory(Exprience::class)->create());
            $user->articles()->save(factory(Article::class)->create());
        });
    }
}
