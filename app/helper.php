<?php
function flash()
{
    $flash = app('App\Http\Flash');
    return $flash;
}

function jsonResp($content, $statusCode, $optionalArg = null)
{
    if ($optionalArg != null) {
        return response($content, $statusCode)->json($optionalArg);
    }
    return response($content, $statusCode);
}
