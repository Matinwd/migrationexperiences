<?php

namespace App\Http\Controllers\API;

use App\Exprience;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

class ExperiencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expriences = Exprience::all();
        return response(['experiences' => $expriences]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->toArray(), [
            'title' => 'required|max:100',
            'description' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()]);
        }

        $exp = new exprience();
        $exp->title = \request('title');
        $exp->description = \request('description');
        $exp->body = \request('body');
        $exp->slug = $this->createslug();
        $exp->image_preview = \request('image_preview');
        $exp->saveorfail();
        return response(['exprience' => $exp], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exp = Exprience::find($id);
        $userExp = $exp->author()->get();
        return response([
            'data' => ['Experiences'=>$exp, 'userExperience' => $userExp]
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $validator = Validator::make(\request()->toArray(), [
            'title' => 'required|max:100',
            'description' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()]);
        }

        $exp = Exprience::find($id);
        $exp->title = \request('title');
        $exp->description = \request('description');
        $exp->body = \request('body');
        $exp->slug = $this->createslug();
        $exp->image_preview = \request('image_preview');
        $exp->author_id = auth()->user()->id;
        $exp->update();
        return response(['exprience' => $exp, 'status' => 'Updated !'], 201);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $exp = Exprience::findOrFail($id);
            $exp->delete();
            return response(['status' => 'Deleted !'], 200);
        } catch (\Exception $e) {
            return response(['status' => 'can not find this experience for delete']);
        }
    }

    private function createSlug()
    {
        $miniSlug = mb_substr(rand(10, 21) . rand(123, 1255) . mb_substr(md5('213'), 0, 6), 0, 6);
        $slug = md5($miniSlug);
        $result = Exprience::where('slug', '=', $slug)->first();
        if (is_null($result)) {
            return $slug;
        }
        return $this->createSlug();
    }
}
