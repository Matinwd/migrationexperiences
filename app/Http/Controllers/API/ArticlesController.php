<?php

namespace App\Http\Controllers\API;

use App\Article;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticlesResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $arts = Article::all();
        $resource = ArticlesResource::collection($arts);
        return $resource;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $this->authorize('isUserAdmin');
        $validator = Validator::make($request->toArray(), [
            'title' => 'required|max:100',
            'description' => 'required|max:255',
            'body' => 'required',
        ]);
        if ($validator->fails()){
            return response(['errors'=>$validator->errors()]);
        }
        $art = new Article();
        $art->title = $request->get('title');
        $art->body = $request->get('body');
        $art->description = $request->get('description');
        $art->image_preview = $request->get('image_preview');
        $art->author_id = auth()->user()->id;
        $art->slug = $this->createSlug();
        $art->saveOrFail();
        return new ArticlesResource($art);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $art = Article::find($id);
        return response(['article' => $art], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->toArray(), [
            'title' => 'required|max:100',
            'description' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()){
            return response(['errors' => $validator->errors()]);
        }

        $art = Article::find($id);
        $art->title = $request->get('title');
        $art->description = $request->get('description');
        $art->body = $request->get('body');
        $art->slug = $this->createSlug();
        $this->image_preview = $request->get('image_preview');
        $this->author_id  = auth()->user()->id;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::find($id)->delete();
    }

    private function createSlug()
    {
        $miniSlug = mb_substr(rand(10, 21) . rand(123, 1255) . mb_substr(md5('213'), 0, 6), 0, 6);
        $slug = md5($miniSlug);
        $result = Article::where('slug', '=', $slug)->first();
        if (is_null($result)) {
            return $slug;
        }
        return $this->createSlug();
    }
}
