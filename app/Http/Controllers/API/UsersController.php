<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Jobs\SendWelcomeMail;
use App\Mail\SendMailable;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsersController extends Controller
{

    public final function login()
    {
        $validator = Validator::make(request()->toArray(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['errors', $validator->errors()]);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => (request('password'))])) {
            $token = auth()->user()->createToken('NewToken')->accessToken;
            return response()->json([
                'token' => $token,
                'code' => 200
            ]);
        } else {
            return response()->json(['error' => 'Unauthorized']);
        }
    }

    public function register()
    {

        $validator = Validator::make(request()->toArray(), [
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required',
            'c_password' => 'required'
        ]);

        if ($validator->fails()) {
            return jsonResp(["errors" => $validator->errors()], 401);
        }
        $usrData = [
            'password' => bcrypt(\request('password')),
            'email' => \request('email'),
            'name' => \request('name')
        ];
        $user = User::create($usrData);
        $this->sendWelcomeEmail($user);
        if ($user) {
            list($tokenResult, $token) = $this->saveToken($user);
            return jsonResp([
                'token' => $tokenResult->accessToken,
                'code' => 200,
                'expires_at' => $token->expires_at->toDateTimeString(),
            ], 200);
        } else {
            return jsonResp(['error' => 'Unauthorized'], 401);
        }
    }

    private function sendWelcomeEmail(User $user)
    {

        Mail::to($user->email)
            ->send(new SendMailable($user->name));
        /*$job = new SendWelcomeMail($user);
        $job->delay(Carbon::now()->addSeconds(5));
        $this->dispatch($job);*/
    }

    /**
     * @param User $user1
     * @return array
     */
    private function saveToken(User $user1): array
    {
        $tokenResult = $user1->createToken('NewToken');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addMonths(2);
        $token->save();
        return [$tokenResult, $token];
    }

}
