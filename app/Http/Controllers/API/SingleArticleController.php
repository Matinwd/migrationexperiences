<?php

namespace App\Http\Controllers\API;

use App\Article;
use App\Exprience;
use App\Http\Controllers\Controller;
use App\Like;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class SingleArticleController extends Controller
{
    public function showUserArticles($email)
    {
        $user = User::where('email', '=', $email)->first();
        $arts = $user->articles()->withCount('likes')->get();
        return response(['data' => ["articles" => $arts, "user" => $user]], 200);
    }

    public function showOneUserArticle($email, $slug)
    {
        $art = Article::where('slug', '=', $slug)->first();
        $user = $art->author;
        $like = $art->withCount('likes')->first();
        return jsonResp(['data' => ["article" => $like,"user" => $user]],
            200);
    }

    public function showUserExperiences($email)
    {
        $user = User::where('email', '=', $email)->first();
        $arts = $user->experiences()->paginate(6)->get();
        return response(['data' => ["articles" => $arts, "user" => $user]], 200);

    }

    public function likeAction($email, $slug)
    {
        if (\request('type') == 'article') {
            $likeable = Article::where('slug', '=', $slug)->first();
//            dd($likeable);
            $type = 'article';
        } else {
            $likeable = Exprience::where('slug', '=', $slug)->first();
            $type = 'experience';
        }

        if (Gate::allows('canUserLikeThisPost', [$likeable, $type])) {
//            dd('awd');
            $this->Like($likeable);
            return jsonResp(["data" => "", "message" => "Liked successfully"], 200);
        }
//        dd('awfwa');
        $this->disLike($likeable, $type);
        return jsonResp(["data" => "", "message" => "disliked successfully"], 200);


    }

    public function viewAction($email ,$slug)
    {
        if(\request('type') == 'article'){
            $post = Article::where('slug' , '=' , $slug)->first();

        }else{
            $post = Exprience::where('slug' , '=' , $slug)->first();
        }
        $post->increment('views');
        $post->save();

        return $post;
    }

    /**
     * @param $likeable
     */
    private function Like($likeable)
    {
        $like = new Like([
            'user_id' => auth()->user()->id,
        ]);
        $like->likeable()->associate($likeable);
        $like->save();

    }

    private function disLike($likeable, $type)
    {
//        dd($type , $likeable);
        $like = new Like([
            "user_id" => auth()->user()->id,
            "likeable_type" => $type,
            "likeable_id" => $likeable->id
        ]);
        $likeable->likes()->delete($like);
    }
}
