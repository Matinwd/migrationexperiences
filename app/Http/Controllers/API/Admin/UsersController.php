<?php

namespace App\Http\Controllers\API\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use App\Mail\SendMailable;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd(\request()->user());
//        $this->authorize('isUserSuperAdmin', auth()->user());
        $users = User::all();
        return response(['users' => $users], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isUserSuperAdmin', auth()->user());

        $validator = Validator::make($request->toArray(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'role' => 'required|in:admin,superAdmin,user,author'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->role = $request->get('role');
        $user->save();
        dd($this->sendWelcomeEmail($user));
        return response(['isSuccess' => true, 'Data' => $user, 'Message' => 'User created successfully!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return ($user ? response(['isSuccess' => true, 'Data' => $user, 'Message' => 'User does exist!'], 200) : response(['isSuccess' => false, 'Message' => 'User does not exist!!']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isUserSuperAdmin', auth()->user());
        $validator = Validator::make($request->toArray(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'role' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->role = $request->get('role');
        $user->update();
        return response(['isSuccess' => true, 'Data' => $user, 'Message' => 'User updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isUserSuperAdmin', auth()->user());
        $user = User::findOrFail($id);
        if ($user != null) {
            $user->delete();
            return response(['isSuccess' => true, 'Message' => 'User deleted successfully!']);
        }
        return response(['isSuccess' => false, 'Message' => 'User deleted failed!']);
    }


    // TODO /** This is already ok but requires setup google account.
    private function sendWelcomeEmail($data)
    {
        Mail::send([], $data->toArray(), function (Message $message) use ($data) {
            $message->to($data->email, $data->name);
            $message->from('697fbf42a8-21a4d2@inbox.mailtrap.io', 'Mohajerat');
            $message->subject('Welcome to our website');
        });
    }
}
