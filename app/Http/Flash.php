<?php

namespace App\Http;
class Flash
{
    public function success($title, $message, $level)
    {
        $this->create($title, $message, $level);
    }

    public function error($title, $message, $level)
    {
        $this->create($title, $message, $level);
    }

    public function create($title, $message, $level)
    {
        session()->flash('flash_message', ['title' => $title, 'message' => $message, 'level' => $level]);
    }


}
