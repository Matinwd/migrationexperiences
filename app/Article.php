<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['author_id'];

    public function author()
    {
        return $this->belongsTo(User::class,'author_id');
    }

        public function likes()
        {
            return $this->morphMany(Like::class,'likes' ,'likeable_type','likeable_id');
        }
}
