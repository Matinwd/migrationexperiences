<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function isUser(User $user)
    {
        return $user->role === 'user';
    }

    public function isUserAuthor(User $user)
    {
        return $user->role === 'author';
    }

    public function isUserAdmin(User $user)
    {
        return $user->role === 'admin';
    }

    public function isUserSuperAdmin(User $user)
    {
        return $user->role === 'superAdmin';
    }
}
