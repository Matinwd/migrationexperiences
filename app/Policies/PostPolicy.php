<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canUserLikeThisPost($user, $post, $type)
    {
            return $this->userNotLikedBefore($user, $post, $type);
    }

    private function userNotLikedBefore($user, $post, $type)
    {
        $res = DB::table('likes')
            ->where('user_id', '=', $user->id)
            ->where('likeable_id', '=', $post->id)
            ->where('likeable_type', '=', $type)->first();
//        dd($res , $user->id , $post->id , $type);
        return $res == null ? true : false;
    }
}
