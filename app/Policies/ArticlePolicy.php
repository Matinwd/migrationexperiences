<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isUserCreatedThisArticle($user,$article)
    {
        return $user->id == $article->user->author_id;
    }

    public function isUserLoggedIn()
    {
        return auth()->check();
    }

    public function canUserLike($user, $article)
    {
        return $this->isUserLoggedIn() && !$this->isUserLikedThisArticle($user,$article);
    }

    public function isUserLikedThisArticle($user,$article)
    {
        return DB::table('article_likes')
            ->where('user_id' , '=' , $user->id )
            ->where('article_id','=',$article->author->author_id)
            ->first() != null ? true : false;
    }
}
