<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exprience extends Model
{
    protected $table = 'expriences';

    protected $fillable = ['title', 'body', 'description', 'slug', 'image_preview', 'author_id'];

    public function author()
    {
        return $this->hasMany(User::class,'id','author_id');
    }

    public function likes()
    {
        return $this->morphMany(Like::class,'likes' ,'likeable_type','likeable_id');
    }


/*
    public function comments()
    {
        return $this->belongsToMany(Comment::class, 'comments', 'user_id', 'post_id');
    }*/
}
