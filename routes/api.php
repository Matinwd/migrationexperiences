<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\HttpLogger\Middlewares\HttpLogger;

Route::group(['namespace' => 'API'], function () {
    Route::post('login', 'UsersController@login');
    Route::post('register', 'UsersController@register');
    Route::group(['prefix' => 'u/@{email}'], function () {
        Route::get('articles', 'SingleArticleController@showUserArticles');
        Route::get('experiences', 'SingleArticleController@showUserExperiences');
        Route::get('article/{slug?}', 'SingleArticleController@showOneUserArticle');
        Route::group(['middleware' => 'auth:api'],function(){
            Route::group(['prefix' => 'article/{slug}'], function () {
                Route::post('/like', 'SingleArticleController@likeAction');
                Route::post('/view', 'SingleArticleController@viewAction');
            });
            Route::group(['prefix' => 'experience/{slug}'], function () {
                Route::post('/like', 'SingleArticleController@likeAction');
                Route::post('/view', 'SingleArticleController@viewAction');
            });
        });
    });
    Route::resource('experiences', 'ExperiencesController')->except(['create', 'edit']);
    Route::resource('articles', 'ArticlesController')->except(['create', 'edit']);
    Route::group(['middleware' => 'auth:api', 'prefix' => 'admin'], function () {
        Route::resource('users', 'Admin\UsersController')->except(['create', 'edit']);
//        Route::post('details', 'UsersController@details');
    });
});



